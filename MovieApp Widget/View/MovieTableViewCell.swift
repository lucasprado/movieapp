//
//  MovieTableViewCell.swift
//  MovieApp Widget
//
//  Created by Lucas Eduardo do Prado on 30/01/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    
    var movie: Movie? {
        didSet {
            titleLabel.text = movie?.title
            if let postPath = movie?.posterPath, let imageURL = URL(string: "https://image.tmdb.org/t/p/w500\(postPath)") {
                let data = try? Data(contentsOf: imageURL)
                posterImageView.image = UIImage(data: data!)
            }
        }
    }
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
