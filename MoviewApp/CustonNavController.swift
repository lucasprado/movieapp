//
//  CustonNavController.swift
//  MovieApp
//
//  Created by Samuel Pinheiro Junior on 19/02/2018.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import UIKit

class CustonNavController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.tintColor = UIColor.white
    }
}
