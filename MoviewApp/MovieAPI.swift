//
//  MovieAPI.swift
//  MovieApp
//
//  Created by Lucas Eduardo do Prado on 28/01/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import Foundation
import Moya

enum MovieAPI {
    case NowPlaying(page: Int)
    case Popular(page: Int)
    case TopRated(page: Int)
    case Credits(movieID: Int)
}

extension MovieAPI: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/") else { fatalError("baseURL could not be configured") }
        return url
    }
    
    var path: String {
        switch self {
        case .NowPlaying:
            return "now_playing"
        case .Popular:
            return "popular"
        case .TopRated:
            return "top_rated"
        case .Credits(let movieID):
            return String(movieID) + "/credits"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
    
    var task: Moya.Task {
        var parameters: [String : Any] = ["api_key": API.apiKey, "language": "pt-BR"]
        
        switch self {
        case .NowPlaying(let page), .Popular(let page), .TopRated(let page):
            parameters["page"] = page
        case .Credits( _): break
            //nada acontece feijoada
        }
        
        return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    
}
