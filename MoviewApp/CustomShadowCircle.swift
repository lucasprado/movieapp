//
//  CustomShadowCicleImage.swift
//  MovieApp
//
//  Created by Samuel Pinheiro Junior on 20/02/2018.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import UIKit

class CustomShadowCircle: UIView {

    override func awakeFromNib(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 0.8
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1.0
    }
}
