//
//  APIResult.swift
//  MovieApp
//
//  Created by Lucas Eduardo do Prado on 28/01/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import Foundation

struct APIResult: Decodable {
    var movies: [Movie]?
    var casts: [Cast]?
    
    enum CodingKeys: String, CodingKey {
        case movies = "results",
             casts = "cast"
    }
    
}
