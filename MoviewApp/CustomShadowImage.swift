//
//  CustomShadowImage.swift
//  MovieApp
//
//  Created by Samuel Pinheiro Junior on 20/02/2018.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import UIKit

class CustomShadowImage: UIImageView {
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 2.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.masksToBounds = true;
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width:0,height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false;
    }

}
