//
//  Cast.swift
//  MovieApp
//
//  Created by Samuel Pinheiro Junior on 20/02/2018.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import Foundation

struct Cast: Codable {
    
    let character: String
    let name: String
    let imagePath: String?
   
    
    enum CodingKeys: String, CodingKey {
        case character, name, imagePath = "profile_path"
    }
}
