//
//  CastCollectionViewCell.swift
//  MovieApp
//
//  Created by Samuel Pinheiro Junior on 20/02/2018.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import UIKit

class CastCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var caracterLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        image.layer.cornerRadius = 40
        image.clipsToBounds = true
    }
    
    
    var cast: Cast?{
        didSet{
            nameLabel.text = cast?.name
            caracterLabel.text = cast?.character
            guard let cast = cast, let path = cast.imagePath, let imageURL = URL(string: "https://image.tmdb.org/t/p/w342\(path)") else { return }
            let data = try? Data(contentsOf: imageURL)
            image.image = UIImage(data: data!)
        }
    }
    
}
