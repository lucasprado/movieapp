//
//  DetailsViewController
//  MovieApp
//
//  Created by Lucas Eduardo do Prado on 29/01/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    var movie: Movie?
    let cellId = "cellId"
    @IBOutlet var collection: UICollectionView!
    @IBOutlet var detailsView: DetailsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailsView.movie = movie
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        API.getCredits(movieID: (movie?.id)!) { (casts) in
            self.movie?.casts = casts
            self.collection.reloadData()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}

extension DetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = movie?.casts?.count{
            return count
        } else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? CastCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.cast = movie?.casts![indexPath.row]
        
        return cell
    }
    
    
}

