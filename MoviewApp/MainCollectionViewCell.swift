//
//  MainCollectionViewCell.swift
//  MovieApp
//
//  Created by Lucas Eduardo do Prado on 28/01/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib(){
        self.layer.cornerRadius = 2.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.masksToBounds = true;
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width:0,height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false;
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.contentView.layer.cornerRadius).cgPath
    }
    
    var movie: Movie? {
        didSet {
            guard let movie = movie, let imageURL = URL(string: "https://image.tmdb.org/t/p/w342\(movie.posterPath)") else { return }
            let data = try? Data(contentsOf: imageURL)
            coverImageView.image = UIImage(data: data!)
           
            titleLabel.text = movie.title
                
        }
    }
}

