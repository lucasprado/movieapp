//
//  API.swift
//  MovieApp
//
//  Created by Lucas Eduardo do Prado on 28/01/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import Foundation
import Moya

class API {
    static let apiKey = "355b6cf80c68346cf635d7740794abbf"
    static let provider = MoyaProvider<MovieAPI>()
    
    static func getPopular(page: Int, completion: @escaping (([Movie]) -> ())) {
        provider.request(.Popular(page: page)) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(APIResult.self, from: response.data)
                    completion(results.movies!)
                } catch let err {
                    print(err)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    static func getTopRated(page: Int, completion: @escaping (([Movie]) -> ())) {
        provider.request(.TopRated(page: page)) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(APIResult.self, from: response.data)
                    completion(results.movies!)
                } catch let err {
                    print(err)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    static func getNowPlaying(page: Int, completion: @escaping (([Movie]) -> ())) {
        provider.request(.NowPlaying(page: page)) { result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(APIResult.self, from: response.data)
                    completion(results.movies!)
                } catch let err {
                    print(err)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    
    static func getCredits(movieID: Int, completion: @escaping (([Cast]) -> ())) {
        provider.request(.Credits(movieID: movieID) ){ result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(APIResult.self, from: response.data)
                    completion(results.casts!)
                } catch let err {
                    print(err)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    
   
    
}
