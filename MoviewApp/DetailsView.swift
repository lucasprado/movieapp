//
//  DetailsView.swift
//  MovieApp
//
//  Created by Lucas Eduardo do Prado on 29/01/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import UIKit

class DetailsView: UIView {
    
    var movie: Movie? {
        didSet {
            guard let movie = movie,
                  let imageURL = URL(string: "https://image.tmdb.org/t/p/w500\(movie.posterPath)"),
                  let backdropURL = URL(string: "https://image.tmdb.org/t/p/w500\(movie.backdrop)") else { return }
            
            rateLabel.text = String(format:"%.1f/10", movie.rating)
            descriptionLabel.text = movie.overview
            dateLabel.text = movie.releaseDate
            titleLabel.text = movie.title
            let data = try? Data(contentsOf: imageURL)
            posterImageView.image = UIImage(data: data!)
            let dataBackdrop = try? Data(contentsOf: backdropURL)
            backdropImageView.image = UIImage(data: dataBackdrop!)
        }
    }

    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
}
