//
//  Movie.swift
//  MovieApp
//
//  Created by Lucas Eduardo do Prado on 28/01/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import Foundation

struct Movie: Codable {
    let id:Int!
    let posterPath: String
    let backdrop: String
    let title: String
    let originalTitle: String
    var releaseDate: String
    var rating: Double
    let overview: String
    var casts: [Cast]?

    enum CodingKeys: String, CodingKey {
        case id, posterPath = "poster_path", backdrop = "backdrop_path", title, originalTitle = "original_title", releaseDate = "release_date", rating = "vote_average", overview, casts
    }
}

