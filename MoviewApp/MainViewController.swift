//
//  MainViewController.swift
//  MovieApp
//
//  Created by Lucas Eduardo do Prado on 27/01/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    // Today
    private let kAppGroupName = "group.ciandt.dojo.MovieApp"
    var page = 1
    private var movies = [Movie]()
    var moviesPlaying: [Movie]?
    var flow = true
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPopuparFilmsAPI(page)
        getNowPlayingAPI()
        gridBuilding()
    }
    
    fileprivate func getPopuparFilmsAPI(_ pageNumber: Int) {
        
        API.getPopular(page: pageNumber, completion: { movies in
            self.movies.append(contentsOf: movies)
            self.collectionView.reloadData()
        })
    }
    // Usado no today
    fileprivate func getNowPlayingAPI() {
        API.getNowPlaying(page: 1, completion: { movies in
            self.moviesPlaying = movies
            UserDefaults.init(suiteName: self.kAppGroupName)?.setValue(try? PropertyListEncoder().encode(movies), forKey: "NowPlaying")
        })
    }
    
    fileprivate func gridBuilding() {
        let width = (view.frame.size.width - 30) / 3
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let height = width * 1.47
        layout.itemSize = CGSize(width: width, height: height)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsSegue" {
            guard let vc = segue.destination as? DetailsViewController,
                  let cell = sender as? MainCollectionViewCell,
                  let indexPath = self.collectionView?.indexPath(for: cell) else { return }
            
            vc.movie = movies[indexPath.row]
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y < -50){
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource  {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCollectionViewCell", for: indexPath) as? MainCollectionViewCell else {
                return UICollectionViewCell()
        }
        cell.movie = movies[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastItem = movies.count - 1
        if indexPath.row == lastItem{
            page = page + 1
            getPopuparFilmsAPI(page)
        }
    }
}




